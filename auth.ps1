Import-Module Azure
Add-AzureAccount

$tenant = 'gavinmcewenplanlogic.onmicrosoft.com'
$graphApiUri = "https://graph.windows.net" # Graph API endpoint

function Get-AuthenticationResult(){
  $clientId = "1950a258-227b-4e31-a9cf-717495945fc2" # Set well-known client ID for Azure PowerShell
  $redirectUri = "urn:ietf:wg:oauth:2.0:oob" # Set redirect URI for Azure PowerShell
  $authority = "https://login.windows.net/$tenant" # Azure Active Directory Tenant
  # Should ensure that ADAL libraries are loaded before this call.
  $authContext = New-Object "Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext" -ArgumentList $authority
  $authResult = $authContext.AcquireToken($graphApiUri, $clientId, $redirectUri, "Auto")
  return $authResult
}

function Get-AADApplication {
    Param(
        [Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationResult][Parameter(Mandatory=$true)] $authResult,
        [Uri][Parameter(Mandatory=$false)] $identifierUri
    )
    $tenant = $authResult.TenantId
    $headers = @{"Authorization" = $authResult.CreateAuthorizationHeader();}
    $uri = "${graphApiUri}/${tenant}/applications?api-version=1.5"
    if ($null -ne $identifierUri) {
        $uri += "&`$filter=identifierUris/any(x:x eq '$identifierUri')"
    }
    $response = Invoke-RestMethod -Uri $uri -Method Get -Headers $headers -ContentType 'application/json'
    return $response.value
}

function Get-AADServicePrincipal {
    Param(
        [Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationResult][Parameter(Mandatory=$true)] $authResult,
        [string] $spn
    )
    $tenant = $authResult.TenantId
    $headers = @{"Authorization" = $authResult.CreateAuthorizationHeader();}
    $uri = "${graphApiUri}/${tenant}/servicePrincipals?api-version=1.5"
    if ($null -ne $spn) {
        $uri += "&`$filter=servicePrincipalNames/any(x:x eq '$spn')"
    }
    $response = Invoke-RestMethod -Uri $uri -Method Get -Headers $headers -ContentType 'application/json'
    return $response.value
}

function Get-AADAppRoleAssignment {
    Param(
        [Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationResult][Parameter(Mandatory=$true)] $authResult,
        [string][Parameter(Mandatory=$true)] $servicePrincipalId
    )
    $tenant = $authResult.TenantId
    $headers = @{"Authorization" = $authResult.CreateAuthorizationHeader();}
    $uri = "${graphApiUri}/${tenant}/servicePrincipals/${applicationId}/appRoleAssignedTo?api-version=1.5"
    $response = Invoke-RestMethod -Uri $uri -Method Get -Headers $headers -ContentType 'application/json'
    return $response.value
}

function Get-AADUser {
    Param(
        [Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationResult][Parameter(Mandatory=$true)] $authResult,
        [string][Parameter(Mandatory=$false)] $userId
    )
    $tenant = $authResult.TenantId
    $headers = @{"Authorization" = $authResult.CreateAuthorizationHeader();}
    $uri = "${graphApiUri}/${tenant}/users"
    if ($null -ne $userId) {
        $uri += "/${userId}?api-version=1.5&`$expand=appRoleAssignments"
    }
    else {
        $uri += "?api-version=1.5"
    }
    $uri = "${graphApiUri}/${tenant}/users/${userId}?api-version=1.5&`$expand=appRoleAssignments"
    $response = Invoke-RestMethod -Uri $uri -Method Get -Headers $headers -ContentType 'application/json'
    return $response
}

function Set-AADAppRoles {
    Param(
        [Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationResult][Parameter(Mandatory=$true)] $authResult,
        [string][Parameter(Mandatory=$true)] $objectId,
        [string][Parameter(Mandatory=$true)] $appRoles
    )
    $tenant = $authResult.TenantId
    $headers = @{"Authorization" = $authResult.CreateAuthorizationHeader();}
    $uri = "${graphApiUri}/${tenant}/applications/${objectID}?api-version=1.5"
    $response = Invoke-RestMethod -Uri $uri -Method Patch -Headers $headers -ContentType 'application/json' -Body "{ ""appRoles"" : ${appRoles} }"
    return $response
}

$authResult = Get-AuthenticationResult

# Copy appRoles from one application to another.
$sourceApp = Get-AADApplication -authResult $authResult -identifierUri 'https://gavinmcewenplanlogic.onmicrosoft.com/WebApp-lims-systest.azurewebsites.net'
$targetAppId = Get-AADApplication -authResult $authResult -identifierUri 'https://gavinmcewenplanlogic.onmicrosoft.com/lims-uat.azurewebsites.net' | select -ExpandProperty objectId
$appRoles = $sourceApp.appRoles | ConvertTo-Json -Depth 32
Set-AADAppRoles -authResult $authResult -objectId $targetAppId -appRoles $appRoles


# Call Azure Service Management REST API to list Azure administrators
$application = Get-AADApplication -authResult $authResult -identifierUri "https://${tenant}/lims-merlin.azurewebsites.net"
$objectId = $response.objectId
$application.appId = $null
$application.passwordCredentials = @()
$application.oauth2Permissions = @()
$application.identifierUris = @("https://${tenant}/LIMS-test.azurewebsites.net")
$application.displayName = "LIMS-test"
$serialized = ConvertTo-Json -InputObject $application -Depth 32
$result = Invoke-RestMethod -Uri "${graphApiUri}/${tenant}/applications?api-version=1.5" -Method Post -Headers $requestHeader -Body $serialized -ContentType 'application/json'
$objectId = $result.objectId
Invoke-RestMethod -Uri "${graphApiUri}/${tenant}/applications/${objectId}?api-version=1.5" -Method Delete -Headers $requestHeader -ContentType 'application/json'

$passwordCredentials = '{
    "passwordCredentials": [
        {
            "customKeyIdentifier":  null,
            "endDate":  "2016-02-12T00:00:00.0000000Z",
            "keyId":  "359f018f-2a13-4208-a24a-490f1170c81b",
            "startDate":  "2015-02-12T00:00:00.0000000Z",
            "value":  "my-password"
        }
    ]
}'

$it = Invoke-RestMethod -Uri "${graphApiUri}/${tenant}/applications/${objectId}?api-version=1.5" -Method Patch -Headers $requestHeader -ContentType 'application/json' -Body $passwordCredentials

# Delete all the appRoleAssignments associated with a user
$user.appRoleAssignments | foreach {
    $tenant = $authResult.TenantId
    $headers = @{"Authorization" = $authResult.CreateAuthorizationHeader();}
    $userId = $_.principalId
    $appRoleAssignmentId = $_.objectId
    $uri = "${graphApiUri}/${tenant}/users/${userId}/appRoleAssignments/${appRoleAssignmentId}?api-version=1.5"
    # Use Invoke-WebRequest instead of Invoke-RestMethod as work-around for TCP connection hang
    # Reported at https://connect.microsoft.com/PowerShell/feedbackdetail/view/836732
    $response = Invoke-WebRequest -Uri $uri -Method Delete -Headers $headers -ContentType 'application/json'
    return $response
    }